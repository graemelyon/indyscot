int r = 50;
int r_center = 100;

float[] yes_pct = {
0.393,  0.512,  0.512,  0.512,  0.518,  0.538,  0.552,  0.565,  0.552,  0.565,  0.583,  0.583,  0.565,  0.565,  0.495,  0.495,  0.495,  0.495,  0.468,  0.468,  0.495,  0.516,  0.554,  0.557,  0.557,  0.557,  0.554,  0.554,  0.525,  0.566,  0.525,  0.519,  0.466,  0.448,  0.448,  0.452,  0.481,  0.481,  0.484,  0.484,  0.484,  0.489,  0.489,  0.489,  0.557,  0.576,  0.591,  0.576,  0.407,  0.407,  0.427,  0.489,  0.489,  0.497,  0.497,  0.592,  0.603,  0.592,  0.576,  0.547,  0.486,  0.486,  0.494,  0.495,  0.526,  0.526,  0.545,  0.457,  0.579,  0.606,  0.606,  0.577,  0.577,  0.545,  0.522,  0.522,  0.565,  0.565,  0.508,  0.508,  0.508,  0.550,  0.508,  0.492,  0.492,  0.492,  0.527,  0.527,  0.535,  0.558,  0.596,  0.596,  0.479,  0.479,  0.532,  0.552,  0.584,  0.555,  0.555,  0.510,  0.510,  0.525,  0.544,  0.525,  0.568,  0.517,  0.454,  0.454,  0.569,  0.569,  0.540,  0.530,  0.506,  0.506,  0.586,  0.586,  0.600,  0.600,  0.600,  0.607,  0.540,  0.443,  0.443,  0.606,  0.645,  0.645,  0.567,  0.567,  0.598,  0.598,  0.538,  0.538,  0.555,  0.567,  0.567,  0.595,  0.595,  0.571,  0.571,  0.602,  0.602,  0.581,  0.527,  0.493,  0.460,  0.460,  0.469,  0.479,  0.586,  0.586,  0.574,  0.574,  0.584,  0.584,  0.575,  0.575,  0.591,  0.591,  0.518,  0.462,  0.462,  0.498,  0.540,  0.578,  0.578,  0.568,  0.541,  0.541,  0.575,  0.575,  0.549,  0.504,  0.496,  0.496,  0.523,  0.536,  0.541,  0.570,  0.570,  0.534,  0.523,  0.523,  0.506};
int N_data = yes_pct.length;

int delay = 2000;
float yes_angle_target = yes_pct[0] * TWO_PI;
float no_angle_target = yes_angle_target;
float yes_angle = 0;
float no_angle = TWO_PI - 0.001;
float yes_angle_prev;
float no_angle_prev;
float dA_yes;
float dA_no;
float dy = 0.3;

float plot_dx;
float xpad = (float)width / 4.0;
float x1, x2, y1, y2;

int slider_pos = 1;
float t_start = 0;
SliderControl myslidercontrol;
TimeLineScroller mytimelinescroller;
PFont font;
int fs = 18;

boolean REDRAW_SKETCH = true;
boolean ANIMATION_DONE = false;
boolean SKETCH_VISIBLE = true;
boolean REDRAWING_SKETCH = false;

float WIDTH_SCALAR = 1;
float HEIGHT_SCALAR = 0.5;

float PIE_HEIGHT = 0.6;
float CENTER_R = 0.6;
float SLIDER_POS_SCALER = 0.15;




void setup() {
    size( WIDTH_SCALAR * $(window).width(), HEIGHT_SCALAR * $(window).height() );
    noStroke();

    plot_dx = (width - (2 * xpad)) / N_data;

    dA_yes = yes_angle_target / delay;
    dA_no = (TWO_PI - no_angle_target) / delay;

    font = loadFont("SourceSansPro-Light.ttf");
    textAlign(CENTER, CENTER);
    textFont(font);


    mytimelinescroller = new TimeLineScroller();

    r = PIE_HEIGHT * height;
    r_center = CENTER_R * r;

    myslidercontrol = new SliderControl(r + SLIDER_POS_SCALER * height);

    smooth();
}


void draw() {


    //Redraw
    if (REDRAW_SKETCH == true && REDRAWING_SKETCH == false) {
        t_start = millis();
        REDRAWING_SKETCH = true;
        delay = 2500;
    }
    t_since = millis() - t_start;

    background(255);

        plotLineGraph();

    translate(width / 2, height / 2);
    rotate(-HALF_PI);


    //SLIDER CHANGED
    if (slider_pos != floor(myslidercontrol.pct_complete * (N_data - 1))) {
        t_start = millis();
        slider_pos = floor(myslidercontrol.pct_complete * (N_data - 1));

        //update targets and angle changes
        yes_angle_target = yes_pct[slider_pos] * TWO_PI;
        no_angle_target = yes_angle_target;
        yes_angle_prev = yes_angle;
        no_angle_prev = no_angle;
        dA_yes = (yes_angle_target - yes_angle_prev) / delay;
        dA_no =  (no_angle_target - no_angle_prev) / delay;
    }


    //Animate
    float t_since = millis() - t_start;
    if (t_since < delay) {
        yes_angle = yes_angle_prev + t_since * dA_yes;
        no_angle =  no_angle_prev + t_since * dA_no;
    } else {
        yes_angle = yes_angle_target;
        no_angle = no_angle_target;
        delay = 150;
        REDRAW_SKETCH = false;
    }


    // Fill clockwise
    fill(0, 0, 255);
    arc(0, 0, r, r, 0, yes_angle );

    // Fille anti-clockwise
    fill(255, 0, 0);
    arc(0, 0, r, r, no_angle, TWO_PI);

    //fill center
    fill(255);
    ellipse(0, 0, -r_center, r_center);

    //SliderControl
    myslidercontrol.isClicked();
    myslidercontrol.display();

    // Text
    rotate(HALF_PI);

    textSize(fs);

    translate(-width / 2, -height / 2);
    float dy = 12;
    String s_yes = (round((100 * yes_pct[slider_pos]))) + "%";
    String s_no = (round((100 * (1 - yes_pct[slider_pos])))) + "%";

    float yes_w = textWidth(s_yes);
    float yes_h = textAscent(s_yes);
    fill(0, 0, 255);
    text(s_yes, width / 2, 0, r_center / 2, height - yes_h);
    // fill(0, 128);
    // rect(width/2-r_center/2, 0, r_center/2, height);
    fill(255, 0, 0);
    text(s_no, width / 2 - r_center / 2, 0, r_center / 2, height - yes_h);



    mytimelinescroller.updatePosition(myslidercontrol.pct_complete);
    mytimelinescroller.display();


    // //Frame
    // stroke(128);
    // line(width / 4, 0, width / 4, height);
    // line(width / 2, 0, width / 2, height);
    // line(3 * width / 4, 0, 3 * width / 4, height);
    // line(0, height / 4, width, height / 4);
    // line(0, height / 2, width, height / 2);
    // line(0, 3 * height / 4, width, 3 * height / 4);
    // noStroke();
}


void plotLineGraph() {

    pushStyle();

    plot_dx =  (width - (2 * (width / 4))) / N_data;

    float yscale = 0.25;


    int slider_p_now = floor(myslidercontrol.pct_complete * (N_data - 1));
    stroke(2, 175);
    x1 = (width / 4) + (slider_p_now) * plot_dx;

    y1 = height - ((0.5 - dy)* height * yscale) - 15;
    y2 = height - ((0.5 - dy) * height * yscale) + 15;

    line(x1, y1, x1, y2);

    for (int i = 1; i < N_data; i++) {

        x1 = (width / 4) + (i - 1) * plot_dx;
        x2 = (width / 4) + i * plot_dx;

        y1 = height - (yes_pct[i - 1] - dy) * height * yscale;
        y2 = height - (yes_pct[i] - dy) * height * yscale;
        stroke(0, 0, 255, 150);
        line(x1, y1, x2, y2);

        y1 = height - (1 - yes_pct[i - 1] - dy) * height * yscale;
        y2 = height - (1 - yes_pct[i] - dy) * height * yscale;
        stroke(255, 0, 0, 150);
        line(x1, y1, x2, y2);
    }
    popStyle();

}



/*****************************************************************/
//*** Slider ****//
/*****************************************************************/
class SliderControl {

    float dist;
    PVector pos;
    float r_slider = 10;
    boolean CLICKED_SLIDER = false;
    float ANGLE_MIN = -QUARTER_PI;
    float ANGLE_MAX = QUARTER_PI;
    float pct_complete = 0;

    SliderControl(float _dist) {
        dist = _dist;
        float dx = -dist * sin(QUARTER_PI) / 2;
        float dy = dist * cos(QUARTER_PI) / 2;
        pos = new PVector(dx, dy);
    }

    void isClicked() {

        if (mousePressed) {
            if ((mouseX - width / 2 >= pos.x - r_slider) && (mouseX - width / 2 <= pos.x + r_slider) &&
                    (mouseY - height / 2 >= pos.y - r_slider) && (mouseY - height / 2 <= pos.y + r_slider)) {
                CLICKED_SLIDER = true;
            }
        } else {
            CLICKED_SLIDER = false;
        }

        //Move slider while mouse still clicked
        if (CLICKED_SLIDER) {
            float angle = atan2(mouseX - width / 2, mouseY - height / 2);
            angle = max(ANGLE_MIN, angle);
            angle = min(ANGLE_MAX, angle);
            pos.x = dist * sin(angle) / 2;
            pos.y = dist * cos(angle) / 2;
            pct_complete = (angle + QUARTER_PI) / HALF_PI;
        }
    }

    void display() {
        stroke(0);
        fill(0, 0);
        arc(0, 0, dist, dist, 3 * QUARTER_PI, 5 * QUARTER_PI);

        fill(128,255);
        pushMatrix();
        rotate(HALF_PI);
        ellipse(pos.x, pos.y, 2 * r_slider, 2 * r_slider);
        popMatrix();
        noStroke();
    }
};


/*****************************************************************/
//*** TimeLineScroller
/*****************************************************************/
class TimeLineScroller {

    float center_pos = width / 2;
    int N = 44 + 1;
    int dx = 20;
    int dy;
    int full_length = N * dx;
    float[] trans = new float[width];
    float dtri = 10;
    String[] months = {"Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"};


    TimeLineScroller() {
        getTrans();
    }

    void display() {

        fill(0);
        noStroke();
        triangle((width / 2) - dtri, 0, width / 2 + dtri, 0, width / 2, dtri);

        //Draw timeline
        pushMatrix();
        stroke(2);
        fill(0);
        textSize(fs / 2);

        translate(center_pos - 10 * dx - 0.5 * dx, 0);

        for (int i = 0; i < N; i++) {
            translate(dx, 0);
            if ((i % 4) == 0) {
                dy = 25;
                text(months[floor(i / 4)], -50, 15, 100, 30);
            } else {
                dy = 10;
            }
            line(0, 0, 0, dy);
        }
        popMatrix();

        //Fade box
        noStroke();
        for (int i = 1; i < width; i++) {
            fill(255, trans[i]);
            rect(i - 1, 0, 1, 25 + 25);
            //println(i);
        }
    }

    void updatePosition(float frac) {
        center_pos = (width / 2) - dx - frac * (full_length - dx - 20 * dx);
    }

    void getTrans() {
        for (int i = 0; i < width; i++) {
            trans[i] = 255.0 * (1.0 - 0.5 * cos(TWO_PI * (i / width)) - 0.5);
            float j = float(i) / float(width);
            float temp = 0.5 * cos(TWO_PI * j) + 0.5;
            trans[i] = min(255, (255 * temp * 4));
        }
    }
};




/*****************************************************************/
// Javascript helpers
/*****************************************************************/
void resizeSketch() {
    size( WIDTH_SCALAR * $(window).width(), HEIGHT_SCALAR * $(window).height() );
    r = PIE_HEIGHT * height;
    r_center = CENTER_R * r;
    REDRAW_SKETCH = true;


    plot_dx = (width - (2 * (width / 4))) / N_data;


    float rnew = r + SLIDER_POS_SCALER * height;
    float dx = -rnew * sin(QUARTER_PI) / 2;
    float dy = rnew * cos(QUARTER_PI) / 2;
    mytimelinescroller.pos = new PVector(dx, dy);

    mytimelinescroller.center_pos = width / 2;
    mytimelinescroller.getTrans();
}

void SketchVisible(boolean visible) {
    if (visible == true) {
        if (SKETCH_VISIBLE == false) {
            REDRAW_SKETCH = true;
            SKETCH_VISIBLE = true;
            // for (Bar b : bars) {
            //     b.reset();
            // }
        }
    } else {
        SKETCH_VISIBLE = false;
    }
}
/*****************************************************************/