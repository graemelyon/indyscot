ArrayList<Bar> bars;
int N_bars = 6;
int pad = 20;
int xsz;
PFont font;
// String[] labels = { "Oil", "Currency", "Poverty", "NHS", "Pensions"};
// float[] counts = {94609, 43364, 15534, 70642, 17011};
String[] labels = { "Oil", "NHS", "Europe","Currency", "Pensions", "Poverty",};
float[] counts = {94609, 70642, 43854, 43364, 25741, 15534};
float alpha = 225;

color[] cols = {
    color(210, 100, 0, alpha),
    color(0, 100, 200, alpha),
    color(75, 75, 75, alpha),
    color(75, 0, 255, alpha),
    color(0, 150, 50, alpha),
    color(200, 30, 165, alpha),
};
float mx;
boolean ANIMATE = false;
int t_start = 0;
boolean REDRAW_SKETCH = true;
boolean SKETCH_VISIBLE = true;
boolean REDRAWING_SKETCH = false;

float WIDTH_SCALAR = 0.66;
float HEIGHT_SCALAR = 0.45;

float y_pad = 25;


void setup() {

    size( WIDTH_SCALAR * $(window).width(), HEIGHT_SCALAR * $(window).height() );

    mx = max(counts);
    font = loadFont("ArialNarrow-Bold-48.vlw");
    textFont(font, 30);
    textAlign(LEFT, CENTER);

    //Create bars
    createBars();

}

void draw() {

    background(255);
    for (Bar b : bars) {
        b.animate();
        b.display();
    }

    if (REDRAW_SKETCH == true && REDRAWING_SKETCH==false) {
        REDRAWING_SKETCH = true;
        for (Bar b : bars) {
            b.reset();
        }
    }


}


void createBars() {

    bars = new ArrayList<Bar>();

    int xsz = (width - (N_bars + 1) * pad) / N_bars;

    for (int i = 0; i < N_bars; i++) {
        counts[i] = (height - 2 * y_pad) * (counts[i] / mx);
    }

    for (int i = 0; i < N_bars; i++) {
        int xpos = (i + 1) * pad + (i) * xsz;
        bars.add( new Bar(
                      labels[i],
                      counts[i],
                      xpos,
                      cols[i],
                      xsz) );
    }
}

/********************************************************/
/* Bar                                            */
/********************************************************/
class Bar {

    int count = 0;
    float delay = 1500;
    float d_count;
    String label;
    int countFinal;
    int xpos;
    color col;
    int xsz;
    float trans = 0;
    float transFinal = 150;
    float t_start = 0;
    float d_trans = 0;

    Bar(String _label, int _countFinal, int _xpos, color _col, int _xsz) {
        label = _label;
        countFinal = _countFinal;
        xpos = _xpos;
        col =  _col;
        xsz = _xsz;
        d_count = countFinal / delay;
        d_trans = transFinal / delay;
    }

    void display() {

        //bar
        noStroke();
        fill(col, trans);
        rect(xpos, height - y_pad, xsz, -count);

        pushMatrix();
        rotate(-HALF_PI);
        fill(0, 200);
        text(label, -height + 2 * y_pad, xpos + xsz / 2);
        popMatrix();
    }

    void animate() {
        float t_since = (millis() - t_start);
        if (t_since <= delay) {
            count = (int)(t_since * d_count);
            trans = (int)(t_since * d_trans);
        } else {
            count = countFinal;
            trans = transFinal;
            REDRAW_SKETCH = false;
        }
    }

    void reset() {
        t_start = millis();
        count = 0;
    }

};


/*****************************************************************/
// Javascript helpers
/*****************************************************************/
void resizeSketch() {
    size( WIDTH_SCALAR * $(window).width(), HEIGHT_SCALAR * $(window).height() );

    int n = 0;
    for (Bar b : bars) {
        b.reset();
        int xsz = max(30, (width - (N_bars + 1) * pad) / N_bars);
        b.xsz = xsz;
        b.xpos = (n + 1) * pad + n * xsz;
        n += 1;
    }
}

void SketchVisible(boolean visible) {

    if (visible == true) {
        if (SKETCH_VISIBLE == false) {
            REDRAW_SKETCH = true;
            SKETCH_VISIBLE = true;
            for (Bar b : bars) {
                b.reset();
            }
        }
    } else {
        SKETCH_VISIBLE = false;
    }
}
/*****************************************************************/
